# -*- coding: utf-8 -*-
"""ASSIGNMENT

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1cE9Ku2srokBxMpiX2iU57FU0nPsJdJxL

1.
"""

#the longest word in the given string with length
#read the input string
str = input("enter a string:")
#split the string to a list of words
word_list = str.split()
#finding the longest word
longest_word = max(word_list , key= len)
#finding the index of longst word
length = len(longest_word)
#printing the longest word and its length
print (longest_word, length)

"""2."""

#to check whether the string is palindrome or symmetrical
#read the input string
string ='khokho'
half=int(len(string)/2)
if len(string)%2==0:
  first_str=string[:half]
  second_str=string[half:]
else:
    first_str=string[:half]
    second_str=string[half+1:]

if first_str==second_str:
  print(string,"is a symmetrical")
else:
  print(string,"is not symmetrical")

if first_str==second_str[::-1]:
  print(string,"is palindrome") 
else:
    print(string,"is not palindrome")

#to check whether the string is palindrome or symmetrical
#read the input string
string ='amaama'
half=int(len(string)/2)
if len(string)%2==0:
  first_str=string[:half]
  second_str=string[half:]
else:
    first_str=string[:half]
    second_str=string[half+1:]

if first_str==second_str:
  print(string,"is a symmetrical")
else:
  print(string,"is not symmetrical")

if first_str==second_str[::-1]:
  print(string,"is palindrome") 
else:
    print(string,"is not palindrome")

"""3."""

#checking whether the string is pangram or not
def check(string):
  alphabet = "abcdefghijklmnopqrstuvwxyz"
  for char in alphabet:
    if char not in str.lower():
         return False

  return True
str = input("enter a string")
if (check(str) == True):
  print("Yes")
else:
    print("No")

#checking whether the string is pangram or not
def check(string):
  alphabet = "abcdefghijklmnopqrstuvwxyz"
  for char in alphabet:
    if char not in str.lower():
         return False

  return True
str = input("enter a string")
if (check(str) == True):
  print("Yes")
else:
    print("No")

"""4."""

#hypen seperated sequence of words
print("hypen a seperated sequence of words")                  #lst=which stores multiple items in single variable
lst=[n for n in input().split('-')]
lst.sort()
print("sorted string is: ")
print('-'.join(lst))

"""5."""

def prime(n):
  f=0
  for i in range(2,(n//2)+1):
    if n%i==0:
      f=1
      break
  return f
num=int(input("enter starting number"))
num1=int(input("enter ending number"))
print("twin prime numbers are ")
for i in range(num,num1+1):
  if prime(i)==0 and prime(i+2)==0:
    print(i," ",i+2);

"""6."""

#doubling the string
string1 = input("enter a string")
string2 = ""
for i in string1:
  string2 = string2+i*2
print("original string is",string1)
print("doubled string is",string2)

"""7."""

n=int(input("enter a number: "))
n1=0
n2=1
count=2
if n<=0:
  print("enter a positive number")
elif n==1:
  print("fibonacci sequence: ",n1)
else:
  print("fibonacci sequence:")
  print(n1)
  print(n2)
  while count<n:
    n3=n1+n2
    print(n3,end=' ')
    count+=1
    n1=n2
    n2=n3

"""8."""

num = int(input("Enter a number: "))
even = 0
odd = 0
while num> 0:
    if num % 2 == 0:
        even += 1
    else:
        odd += 1
    num = num // 10

print("even:",even)
print("odd:",odd)

"""9."""

num=int(input("enter a number: "))
sum=0
for i in range(1,num):
  if num%i==0:
    sum=sum+i
if sum==num:
  print("perfect number")
else:
  print("not a perfect number")

"""10."""

def print_even(data):
  print("even length words are...")
  for word in data.split(" "):
    if len(word) %2 ==0:
      print(word, end=" ")

text= input("enter a string:")
print_even(text)

"""11. #DIFFICULTY LEVEL"""

n = int(input("Enter number of rows: "))
for i in range(1, n+1):
    for j in range(0, n-i+1):
        print(' ', end='')
    C = 1
    for j in range(1, i+1):
        print(' ', C, sep='', end='')
        C = C * (i - j) // j
    print()

"""12."""

#get input from user
user_input=input("enter numbers seperated by commas:")
#split input by commas and remove any spaces
numbers=user_input.split(",")
numbers=[num.strip() for num in numbers]
#calculate sum of all numbers
total=0
for num in numbers:
  total+=float(num)
#print user input and sum
print("input:",user_input)
print("sum:",total)

"""13"""

def MDR(n):
    n = str(n)
    product = 1
    for digit in n:
        product *= int(digit)
    if len(str(product)) == 1:
        return product
    else:
        return MDR(product)

def Mpersistence(n):
    persistence = 0
    while len(str(n)) > 1:
        n = MDR(n)
        persistence += 2
    return persistence
n=int(input("Enter the number"))
root = MDR(n)
persistence = Mpersistence(n)
print("MDR is {} and M persistance is {}".format(root,persistence))

"""14."""

import math

def calculate_area(shape, length, radius):
    if shape == "SQR":
        # Calculate the area of the largest inscribed square
        diagonal = length * math.sqrt(2)
        side = diagonal / math.sqrt(2)
        area = side ** 2
        return area
    elif shape == "CI":
        # Calculate the area of the circle that fits within the square and intersects the four circles
        circle_area = math.pi * radius ** 2
        square_area = length ** 2
        intersection_area = (circle_area * 2) - (math.pi * radius ** 2)
        area = square_area - intersection_area
        return area
    else:
        # Invalid shape input
        return -1
print("Area of square=",calculate_area("SQR", 10, 0)) 
print("Area of circle=",calculate_area("CI",10,1))

"""15."""

O=list(map(int,input().split()))
i=1
l=len(O)
while(i<=l):
  if i==O[i-1]:
    print("HIT "+str(i))
    O=O[i:]+O[:i-1]
    l-=1
    i=1
  else:
      i+=1

"""16."""

def coordinates(x1,y1,x2,y2,x3,y3,x,y):
  delta=(y2-y3)(x1-x3)+(x3-x2)(y1-y3)
  u=((y2-y3)(x-x3)+(x3-x2)(y-y3))/delta
  v=((y3-y1)(x-x3)+(x1-x3)(y-y3))/delta
  w=1-u-v
  return u,v,w
def point_inside_triangle(x1,y1,x2,y2,x3,y3,x,y):
  u,v,w=coordinates(x1,y1,x2,y2,x3,y3,x,y)
  return u>=0 and v>=0 and w>=0
n=int(input("Enter the n value:"))
x1,y1=map(float,input().split())
x2,y2=map(float,input().split())
x3,y3=map(float,input().split())
for i in range(n):
  x,y=map(float,input().split())
  if point_inside_triangle(x1,y1,x2,y2,x3,y3,x,y):
    print("INSIDE")
  else:
    print("OUTSIDE")

"""17."""

s=input("enter text:")
words=s.split()
sen=""
l=80
for i in words:
  sen=sen+i+" "
  if len(sen)>1:
    sen=sen.replace(i,"")
    print(sen)
    l=len(sen)
    sen=i+" "
  elif len(sen)<1 and i==words[-1]:
      sen=sen.replace(i,"")
      print(sen+"n"+i)
  else:
        pass

"""18."""

num_subjects = int(input("Enter the number of subjects: "))
subjects = []
students = []
marks = []

for i in range(num_subjects):
    subject_name = input(f"Enter the name of subject {i+1}: ")
    subjects.append(subject_name)
    student_name = input(f"Enter the name of student who scored highest in {subject_name}: ")
    students.append(student_name)
    mark = int(input(f"Enter the marks scored by {student_name} in {subject_name}: "))
    marks.append(mark)

final_list = []
for i in range(num_subjects):
    subject_dict = {}
    subject_dict[subjects[i]] = {students[i]: marks[i]}
    final_list.append(subject_dict)

print(f"Number of Subjects = {num_subjects}")
print(f"Subjects = {subjects}")
print(f"Students = {students}")
print(f"Marks = {marks}")
print(f"Final List = {final_list}")

"""19."""

l=str.upper(input("enter a roman number"))
d={'I':1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000}
sum=0
for i in range(0,len(l)):
  if i+1!=len(l) and d[l[i]]<d[l[i+1]]:
    sum=sum-d[l[i]]
  else:
    sum=sum+d[l[i]]
print(sum)

"""20."""

def removing_values(array,value):
  while value in array:
    array.remove(value)
  return len(array)
array=[2,6,8,9,9,11,256,8]
length=len(array)
value=int(input("enter the value that you want to remove"))
new_length=removing_values(array,value)
print("new length is:",new_length)
array+=['+']*(length-new_length)
print(array)

"""21."""

L=[1,8,9]
num=int("".join(map(str,L)))
max=L[0]
for i in range(0,len(L)):
  if L[i]>max:
    max=L[i]
if max>=0:
  num+=1
  digits=[]
  while num>0:
    digits.append(num%10)
    num//=10
print(digits[::-1])